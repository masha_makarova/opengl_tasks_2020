

#include "./common/Mesh.hpp"
#include "./common/Application.hpp"
#include "./common/ShaderProgram.hpp"
#include <glm/simd/geometric.h>
#include <vector>
#include <algorithm>
#include <cmath>
#include <iostream>

class SurfaceApplication :
        public Application
{
public:
    void makeScene() override
    {
        Application::makeScene();

        //_cameraMover = std::make_shared<FreeCameraMover>();

        _surface = generateSurface(1.0, N);
        _surface->setModelMatrix(glm::mat4(1.0f));
        _shader = std::make_shared<ShaderProgram>("692MakarovaData1/shader.vert", "692MakarovaData1/shader.frag");
    }

    void draw() override
    {
        Application::draw();

        int width, height;
        glfwGetFramebufferSize(_window, &width, &height);

        glViewport(0, 0, width, height);

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        _shader->use();

        _shader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
        _shader->setMat4Uniform("projectionMatrix", _camera.projMatrix);

        _shader->setMat4Uniform("modelMatrix", _surface->modelMatrix());
        _surface->draw();
    }

    void handleKey(int key, int scancode, int action, int mods) override
    {
        Application::handleKey(key, scancode, action, mods);
        if (action == GLFW_PRESS) {
            if (key == GLFW_KEY_MINUS ) {
                updateSurface(-2);
            } else if (key == GLFW_KEY_EQUAL) {
                updateSurface(2);
            }
        }
    }

private:
    MeshPtr generateSurface(float A, int N)
    {
        std::vector<glm::vec3> vertices;
        std::vector<glm::vec3> normals;

        float dU = 2 * glm::pi<float>() / (float)N;

        for (int i = 0; i < N; ++i) {
            float curr_u = dU * i / 4;
            std::cout << curr_u << std::endl;
            float next_u = dU * (i+1) / 4;
            for (int j = 0; j < N; ++j) {
                float curr_v = dU * j;
                float next_v = dU * (j+1);

                glm::vec3 a = makePoint(curr_u, curr_v, A);
                glm::vec3 b = makePoint(curr_u, next_v, A);
                glm::vec3 c = makePoint(next_u, next_v, A);
                glm::vec3 d = makePoint(next_u, curr_v, A);

                glm::vec3 e = makeOpPoint(curr_u, curr_v, A);
                glm::vec3 f = makeOpPoint(curr_u, next_v, A);
                glm::vec3 g = makeOpPoint(next_u, next_v, A);
                glm::vec3 h = makeOpPoint(next_u, curr_v, A);

                vertices.emplace_back(a);
                vertices.emplace_back(b);
                vertices.emplace_back(c);

                vertices.emplace_back(c);
                vertices.emplace_back(d);
                vertices.emplace_back(a);

                glm::vec3 normal = calculateNormal(a,b,c);

                for (int k = 0; k < 6; ++k)
                    normals.emplace_back(normal);

                //---------------------------

                vertices.emplace_back(e);
                vertices.emplace_back(f);
                vertices.emplace_back(g);

                vertices.emplace_back(g);
                vertices.emplace_back(h);
                vertices.emplace_back(e);

                normal = calculateNormal(e,f,g);

                for (int k = 0; k < 6; ++k)
                    normals.emplace_back(normal);

            }

        }
        DataBufferPtr buf0 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
        buf0->setData(vertices.size() * sizeof(float) * 3, vertices.data());

        DataBufferPtr buf1 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
        buf1->setData(normals.size() * sizeof(float) * 3, normals.data());

        MeshPtr mesh = std::make_shared<Mesh>();
        mesh->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, 0, buf0);
        mesh->setAttribute(1, 3, GL_FLOAT, GL_FALSE, 0, 0, buf1);
        mesh->setPrimitiveType(GL_TRIANGLES);
        mesh->setVertexCount(vertices.size());
        return mesh;
    }

    glm::vec3 makePoint(float u, float v, float A)
    {
        return glm::vec3(A*sin(u)*cos(v), A*sin(u)*sin(v), A*(log(tan(u/2)) + cos(u)));
    }

    glm::vec3 makeOpPoint(float u, float v, float A)
    {
        return glm::vec3(A*sin(u)*cos(v), A*sin(u)*sin(v), -A*(log(tan(u/2)) + cos(u)));
    }

    glm::vec3 calculateNormal(glm::vec3 a, glm::vec3 b, glm::vec3 c) {
        glm::vec3 result;

        glm::vec3 vec1(b[0]-a[0], b[1]-a[1], b[2]-a[2]);
        glm::vec3 vec2(c[0]-a[0], c[1]-a[1], c[2]-a[2]);

        result[0] = vec1[1]*vec2[2] - vec1[2]*vec2[1];
        result[1] = vec1[2]*vec2[0] - vec1[0]*vec2[2];
        result[2] = vec1[0]*vec2[1] - vec1[1]*vec2[0];

        if (a[2] < 0) {
            result[0] = -1 * result[0];
            result[1] = -1 * result[1];
            result[2] = -1 * result[2];
        }

        return normalize(result);
    }

    void updateSurface(int dN)
    {
        N += dN;
        _surface = generateSurface(1.0, N);
        _surface->setModelMatrix(glm::mat4(1.0f));
        draw();
    }
public:
    MeshPtr _surface;
    ShaderProgramPtr _shader;

private:
    unsigned int N = 20;
};

int main() {
    SurfaceApplication app;
    app.start();
    return 0;
}
