add_compile_definitions(GLM_ENABLE_EXPERIMENTAL)

set(SRC_FILES
        common/Application.cpp
        common/DebugOutput.cpp
        common/Camera.cpp
        common/Mesh.cpp
        common/ShaderProgram.cpp
        surface.cpp
        )

set(HEADER_FILES
        common/Application.hpp
        common/DebugOutput.h
        common/Camera.hpp
        common/Mesh.hpp
        common/ShaderProgram.hpp
        )

MAKE_OPENGL_TASK(692Makarova 1 "${SRC_FILES}")


